// Q1 write a program in JS to print all the even numbers from the following array and
// must stop printing if a number, multiple of either 5 or 10 is found
// Input arrays a) [ 16, 1, 11, 24, 14, 22, 19, 18, 31, 20, 62, 98, 13]
// 	 .      b) [ 91, 24, 8, 67, 32, 51, 55, 6, 74, 12 ]

var inputA = [ 16, 1, 11, 24, 14, 22, 19, 18, 31, 20, 62, 98, 13];

var inputB = [ 91, 24, 8, 67, 32, 51, 55, 6, 74, 12 ];


for(item of inputA){
	if (item % 5 ==0) {
		break;
	}
	if (item%2==0) {
		console.log(item);
	}
}

for(item of inputB){
	if (item % 5 ==0) {
		break;
	}
	if (item%2==0) {
		console.log(item);
	}
}